# dotfiles

These are my dotfiles for Arch, I'm using [GNU Stow](https://www.gnu.org/software/stow/) to manage them.

For now, I don't want to run a script to install a bunch of packages, so each package needs to be installed individually. Packages needed for a certain task be explicited on the commit message for that file.

## creating symlinks

Just install stow with `$ sudo pacman -S stow` and create symlinks with `$ stow -v -R -t ~ folder_name`.
