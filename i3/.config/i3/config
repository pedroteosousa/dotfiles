# i3 config file (v4)

set $mod Mod4

font pango:FreeMono 8

# start termite
bindsym $mod+Return exec termite

# kill focused window
bindsym $mod+Shift+q kill

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# start xbindkeys
exec xbindkeys

# set capslock to work as escape key (use caps:swapescape to swap keys)
exec setxkbmap -option caps:escape

# --- MENU ---
bindsym $mod+s exec rofi -show run
# ------------

# --- FOCUS ---
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

bindsym $mod+a focus parent 
bindsym $mod+d focus child

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle
# -------------

# --- MOVEMENT ---
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right
# ----------------

# --- SPLIT ---
bindsym $mod+backslash split h
bindsym $mod+minus split v
# -------------

# --- WINDOW LAYOUTS ---
# change container layout (stacked, tabbed, toggle split)
bindsym $mod+t layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split
# ----------------------

# --- FLOATING WINDOWS ---
# toggle tiling / floating
bindsym $mod+Shift+space floating toggle
# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod
# ------------------------

# --- WORKSPACES ---
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7 "
set $ws8 "8 "
set $ws9 "9 "
set $ws10 "0"

# switch to workspace
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+0 move container to workspace number $ws10

assign [class="Trello"] $ws7
assign [class="TelegramDesktop"] $ws8
# spotify doesn't set hints properly (https://github.com/i3/i3/issues/2060)
for_window [class="Spotify"] move to workspace $ws9
# ------------------

# --- I3 STUFF ---
bindsym $mod+Shift+c reload
bindsym $mod+Shift+r restart
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'End X session?' -B 'Yes' 'i3-msg exit'"
# ----------------

# --- RESIZE MODE ---
mode "resize" {
        bindsym h resize shrink width 10 px or 10 ppt
        bindsym j resize grow height 10 px or 10 ppt
        bindsym k resize shrink height 10 px or 10 ppt
        bindsym l resize grow width 10 px or 10 ppt

        bindsym Shift+h resize shrink width 5 px or 5 ppt
        bindsym Shift+j resize grow height 5 px or 5 ppt
        bindsym Shift+k resize shrink height 5 px or 5 ppt
        bindsym Shift+l resize grow width 5 px or 5 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}
bindsym $mod+r mode "resize"
# -------------------

# --- APPEARANCE ---
# colors from gruvbox (https://github.com/morhetz/gruvbox)
set $bg #282828
set $fg #ebdbb2
set $red #cc241d

#                       border | background | text | indicator | child border
client.focused          $fg      $fg          $bg   $fg          $fg
client.focused_inactive $bg      $bg          $fg   $bg          $bg
client.unfocused        $bg      $bg          $fg   $bg          $bg
client.urgent           $red     $red         $fg   $red         $red
# ------------------

# --- BAR ---
bar {
    # use i3blocks
    status_command i3blocks -c ~/.config/i3/i3blocks.conf

    font pango:FreeMono 9

    colors {
        background $bg

        # workspace icons       border | background | text
        focused_workspace       $fg      $fg          $bg
        inactive_workspace      $bg      $bg          $fg
        active_workspace        $bg      $bg          $fg
        urgent_workspace        $bg      $red         $fg
    }
}
# -----------

# remove window title unless in tiling or tab mode
default_border pixel 1
