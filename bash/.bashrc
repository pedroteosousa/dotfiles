# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# --- ALIASES ---
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# --- PS1 ---
BOLD="$(tput bold)"
GREEN="$(tput setaf 2)"
YELLOW="$(tput setaf 3)"
BLUE="$(tput setaf 4)"
PURPLE="$(tput setaf 5)"
RESET="$(tput sgr0)"

if [ -f /usr/share/git/completion/git-prompt.sh ]; then
    . /usr/share/git/completion/git-prompt.sh
fi

git_branch_color() {
    if [ -z "$(git status --porcelain 2> /dev/null)" ]; then
        echo "${GREEN}"
    else
        echo "${YELLOW}"
    fi
}

export PS1='${BOLD}${GREEN}\u${RESET}${BOLD}@${PURPLE}\h ${BLUE}\w$(git_branch_color)$(__git_ps1 " [%s]")${RESET}\n\$ '

# --- PATH ---
PATH=$PATH:~/scripts/
PATH=$PATH:~/.config/i3/blocks/scripts/
PATH=$PATH:~/.npm/bin
PATH=$PATH:~/.local/bin
PATH=$PATH:~/go/bin
