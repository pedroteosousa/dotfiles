set tabstop=4 softtabstop=4 shiftwidth=4 expandtab
set autoindent
set number
set list listchars=eol:$,trail:~,tab:>>

set cursorline

" plugins
call plug#begin()
    Plug 'ap/vim-buftabline'
    Plug 'neoclide/coc.nvim', { 'branch': 'release' }
    Plug 'morhetz/gruvbox'
    Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install'  }
    Plug 'preservim/nerdtree'
    Plug 'simeji/winresizer'
call plug#end()

" golang

autocmd FileType go set noexpandtab

" splits
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" buffers
set autoread
autocmd FocusGained * checktime

set hidden
nnoremap <C-N> :bnext<CR>
nnoremap <C-P> :bprev<CR>

" theme
set background=dark
colorscheme gruvbox
let g:gruvbox_contrast_dark='hard'

" coc.nvim config
let g:coc_global_extensions = ['coc-json', 'coc-tsserver', 'coc-python']

set signcolumn=yes
set updatetime=300

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1] =~# '\s'
endfunction

nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gr <Plug>(coc-references)

" NERDtree config
map <silent> <C-m> :NERDTreeFocus<CR>
let NERDTreeShowHidden=1
let NERDTreeQuitOnOpen=1

" navigation maps
nnoremap j gj
nnoremap k gk
